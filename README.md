# Note

The mission simulator code here is experimental and freely provided, but has not been designed as a complete, outward-facing analysis package. 
Users may encounter various issues such as reliance on external files, redundant functions, etc. 
It is strongly recommended to get in touch with the code authors before attempting to use the simulation code.

Future work may provide a cleaner interface for using these simulations.


# Mission simulator

Estimates flare observation efficiencies given a set of mission parameters.
The idea is to take the observations of flares in solar cycle 24, project them on
to the next solar cycle, and then see how the configuration of a mission affects
the number of flares that can be observed.

The major assumption in this work is that the next solar cycle will be the same
as the previous solar cycle.  Applying some randomness to different parameters
in the simulation, and running multiple simulations, allows us to generate the
likely flare observation behaviour.

# Setting up the code
data/

 - stores the data required to simulate the flare observing properties of a mission
 - goes_flares.hdf5 : ?
 - mission_parameters_*.csv : basic information about the mission including which *_orbit_events.csv file to use, start and end time and FOV.
 - swpc_ars.csv : daily NOAA AR number(s), AR location(s) and McIntosh classification(s)
 - *_orbit_events.csv : times of SAA, eclipse entry/exit, and ground contact
 - major_flare_watches.csv : when and where a major flare watch was called.

 
flare_mission_sim/\_\_init__.py

 - sets up logging output
 - sets and loads the mission configuration file.

# Running the code
%run scripts/simulate_point_mc.py -n N

 - randomizes aspects of the the mission and calculates the number of flares detected
 - generates output that can be used to generate probability distributions
 - command line argument "-n" defines the number of random simulations to create
 
%run scripts/make_summary_mc_plots.py -t "YYYYMMDD_hhmm"

 - reads the output from scripts/simulate_point_mc.py
 - generates plots that statistically summarize the flare observing characteristics of the mission.
 - command line argument "-t" passes in a timestamp that defines which simulation results to load.
